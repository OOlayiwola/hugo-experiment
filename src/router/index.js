import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/pwa/customer',
    name: 'pwaCustomer',
    component: () => import('../views/pwa/pwaCustomer.vue')
  },
  {
    path: '/pwa/customer/loan-application',
    name: 'pwaLoanApp',
    component: () => import('../views/pwa/loanApplication.vue')
  },
  {
    path: '/pwa/customer/loans',
    name: 'pwaLoans',
    component: () => import('../views/pwa/loans.vue')
  },
  {
    path: '/customer',
    name: 'customer',
    component: () => import('../views/customer/customer.vue')
  },
  {
    path: '/customer/loan-application',
    name: 'loanApplication',
    component: () => import('../views/customer/loanApplication.vue')
  },
  {
    path: '/customer/loans',
    name: 'loans',
    component: () => import('../views/customer/loans.vue')
  },
  {
    path: '/profile/:id',
    name: 'profile',
    component: () => import('../views/bank/profile.vue')
  },
  {
    path: '/bank',
    name: 'bank',
    component: () => import('../views/bank/bank.vue')
  },
  {
    path: '/bank/rules',
    name: 'rules',
    component: () => import('../views/bank/rules.vue')
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('../views/dashboard/dashboard.vue')
  },
  {
    path: '/dashboard/app',
    name: 'app',
    component: () => import('../views/dashboard/app.vue')
  },
  {
    path: '/auth',
    name: 'Auth',
    component: () => import('../views/auth.vue')
  },
  {
    path: '/crowdfund',
    name: 'crowdfund',
    component: () => import('../views/crowdfund/crowdfund.vue')
  },
  {
    path: '/crowdfund/proposal',
    name: 'proposal',
    component: () => import('../views/crowdfund/proposal.vue')
  },
  {
    path: '/crowdfund/oss',
    name: 'oss',
    component: () => import('../views/crowdfund/oss.vue')
  },
  {
    path: '/crowdfund/oss/:id',
    name: 'ossDetail',
    component: () => import('../views/crowdfund/ossDetail.vue')
  },
  {
    path: '/crowdfund/community',
    name: 'community',
    component: () => import('../views/crowdfund/community.vue')
  },
  {
    path: '/crowdfund/community/:id',
    name: 'communityDetail',
    component: () => import('../views/crowdfund/communityDetail.vue')
  },
  {
    path: '/crowdfund/orgs',
    name: 'orgs',
    component: () => import('../views/crowdfund/orgs.vue')
  },
  {
    path: '/crowdfund/orgs/:id',
    name: 'orgsDetail',
    component: () => import('../views/crowdfund/orgsDetail.vue')
  },
  {
    path: '/crowdfund/projects',
    name: 'projects',
    component: () => import('../views/crowdfund/projects.vue')
  },
  {
    path: '/crowdfund/projects/:id',
    name: 'projectDetail',
    component: () => import('../views/crowdfund/projectDetail.vue')
  },
  {
    path: '/lending',
    name: 'lending',
    component: () => import('../views/lending/lending.vue')
  },
  {
    path: '/lending/refugee',
    name: 'refugees',
    component: () => import('../views/lending/refugees.vue')
  },
  {
    path: '/lending/refugee/:id',
    name: 'refugeeDetails',
    component: () => import('../views/lending/refugeeDetails.vue')
  },
  {
    path: '/lending/agriculture/:id',
    name: 'agricultureDetails',
    component: () => import('../views/lending/agricultureDetails.vue')
  },
  {
    path: '/lending/education/:id',
    name: 'educationDetails',
    component: () => import('../views/lending/educationDetails.vue')
  },
  {
    path: '/lending/health/:id',
    name: 'healthDetails',
    component: () => import('../views/lending/healthDetails.vue')
  },
  {
    path: '/lending/shelter/:id',
    name: 'shelterDetails',
    component: () => import('../views/lending/shelterDetails.vue')
  },
  {
    path: '/lending/technology/:id',
    name: 'technologyDetails',
    component: () => import('../views/lending/technologyDetails.vue')
  },
  {
    path: '/lending/women/:id',
    name: 'womenDetails',
    component: () => import('../views/lending/womenDetails.vue')
  },
  {
    path: '/lending/borrow',
    name: 'borrow',
    component: () => import('../views/lending/borrow.vue')
  },
  {
    path: '/lending/borrower/minimum-eligibility',
    name: 'borrower',
    component: () => import('../views/lending/borrower.vue')
  },
  {
    path: '/lending/borrower/minimum-eligibility/proposal',
    name: 'Proposal',
    component: () => import('../views/lending/borrower/proposal.vue')
  },
  {
    path: '/lending/my/borrower/application',
    name: 'application',
    component: () => import('../views/lending/application.vue')
  },
  {
    path: '/dao',
    name: 'DAO',
    component: () => import('../views/dao/dao.vue')
  },
  {
    path: '/dao/app',
    name: 'overview',
    component: () => import('../views/dao/app/overview.vue')
  },
  {
    path: '/dao/app/discussions',
    name: 'discussions',
    component: () => import('../views/dao/app/discussions.vue')
  },
  {
    path: '/dao/app/post/create',
    name: 'post',
    component: () => import('../views/dao/app/discussion/post.vue')
  },
  {
    path: '/dao/app/democracy/proposals',
    name: 'proposals',
    component: () => import('../views/dao/app/democracy/proposals.vue')
  },
  {
    path: '/dao/app/democracy/referenda',
    name: 'referenda',
    component: () => import('../views/dao/app/democracy/referenda.vue')
  },
  {
    path: '/dao/app/techcomm/proposals',
    name: 'techProposals',
    component: () => import('../views/dao/app/techcomm/techProposals.vue')
  },
  {
    path: '/dao/app/treasury/proposals',
    name: 'treasuryProposals',
    component: () => import('../views/dao/app/treasury/treasuryProposals.vue')
  },
  {
    path: '/dao/app/social/videochat',
    name: 'video',
    component: () => import('../views/dao/app/social/video.vue')
  },
  {
    path: '/dao/app/members',
    name: 'members',
    component: () => import('../views/dao/app/members.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

import { defineStore } from 'pinia'
import { ethers } from 'ethers'
import Web3 from 'web3'
import { stringify } from 'flatted'
import { abi } from '@/contracts/abiopt'
//import { ref, computed, watch } from "@vue/composition-api";
//import { useLocalStorage } from "@vueuse/core";

export const useAccountStore = defineStore('accountStore', {
  state: () => ({
    selectedChain: undefined,
    provider: undefined,
    signer: undefined,
    library: undefined,
    walletAddress: undefined,
    shortWalletAddress: undefined,
    account: undefined,
    accounts: undefined,
    network: undefined,
    chainId: undefined,
    isConnected: false,
    nativeCurrency: 'ETH',
    OPT_CONTRACT_ADDRESS: '0x3900779428f232fd63382818012f4fde06bdbb1d',
    POLY_CONTRACT_ADDRESS: '0xc3e01986aaf00988fc270db40d999f00784b8553',
    CONTRACT_ADDRESS: "",
    contract: undefined,
    items: ['Optimism', 'Mumbai'],
    select: { name: 'Optimism Kovan', chain: '69' },
    /* items: [
        { name: 'Optimism Kovan', chain: '69' },
        { name: 'Mumbai', chain: '80001' },
      ], */
    networks: {
      Optimism: {
        chainId: `0x${Number(69).toString(16)}`,
        chainName: 'Optimism Kovan',
        nativeCurrency: {
          name: 'Kovan Ether',
          symbol: 'KOR',
          decimals: 18,
        },
        rpcUrls: ['https://kovan.optimism.io'],
        blockExplorerUrls: ['https://kovan-optimistic.etherscan.io'],
      },
      Mumbai: {
        chainId: `0x${Number(80001).toString(16)}`,
        chainName: 'Mumbai',
        nativeCurrency: {
          name: 'MATIC',
          symbol: 'MATIC',
          decimals: 18,
        },
        rpcUrls: [
          'https://matic-mumbai--jsonrpc.datahub.figment.io/apikey/7e384a82b193418b6ed0e8a83331acc7',
          'https://rpc-mumbai.matic.today',
          'https://matic-mumbai.chainstacklabs.com',
          'https://rpc-mumbai.maticvigil.com',
          'https://matic-testnet-archive-rpc.bwarelabs.com',
        ],
        blockExplorerUrls: ['https://mumbai.polygonscan.com/'],
      },
    },
  }),

  getters: {
    getWalletAddress(state) {
      return state.walletAddress
    },
    //setWalletAddress: (state) => (address) => state.walletAddress = address,
  },

  actions: {
    directToChain() {
      console.log('Label: ', this.selectedChain)
    },
    async setSelected(value) {
      //  trigger a mutation, or dispatch an action
      console.log('Label: ', this.selectedChain)
      await this.handleNetworkSwitch(this.selectedChain)
    },
    async handleNetworkSwitch(networkName) {
      await this.changeNetwork(networkName)
    },
    async changeNetwork(networkName) {
      try {
        if (!window.ethereum) throw new Error('No crypto wallet found')

        //let s = ...this.networks[networkName];

        await window.ethereum.request({
          method: 'wallet_addEthereumChain',
          params: [
            {
              ...this.networks[networkName],
            },
          ],
        })

        const provider = window.ethereum
        const library = new ethers.providers.Web3Provider(provider)
        //const accounts = await library.listAccounts()
        const accounts = await provider.enable()
        const network = await library.getNetwork()
        const signer = provider.getSigner()
        this.provider = provider
        this.signer = signer
        this.library = library
        console.log(` Account JS #102 - account: ${accounts}`)
        if (accounts) {
          this.walletAddress = accounts[0]
          this.shortWalletAddress = this.truncateAddress(this.walletAddress)
          this.accounts = accounts
        }

        this.chainId = network.chainId
        this.network = network
        this.isConnected = true

        if (this.chainId == 80001) {
          this.nativeCurrency = 'MATIC'
          this.CONTRACT_ADDRESS = POLY_CONTRACT_ADDRESS
          this.contract = new ethers.Contract(
            this.CONTRACT_ADDRESS,
            abi,
            this.signer,
          )
        } else {
          this.nativeCurrency = 'ETH'
          this.CONTRACT_ADDRESS = OPT_CONTRACT_ADDRESS
          this.contract = new ethers.Contract(
            this.CONTRACT_ADDRESS,
            abi,
            this.signer,
          )
        }

        console.log(` - chainId: ${this.chainId}`)
        console.log(` - Native Currency: ${this.nativeCurrency}`)
        console.log(` - network: ${this.network}`)
        console.log(` - walletAddress #120 ${this.walletAddress}`)
        console.log(` - Short WalletAddress #121 ${this.shortWalletAddress}`)
      } catch (error) {}
    },

    async connect() {
      try {
        let networkName = 'Optimism'
        if (!window.ethereum) throw new Error('No crypto wallet found')

        await window.ethereum.request({
          method: 'wallet_addEthereumChain',
          params: [
            {
              ...this.networks[networkName],
            },
          ],
        })

        const provider = window.ethereum
        const library = new ethers.providers.Web3Provider(provider)
        //const accounts = await library.listAccounts()
        const accounts = await provider.enable()
        const network = await library.getNetwork()
        this.provider = provider
        const signer = provider.getSigner()
        this.signer = signer
        this.library = library
        console.log(` Account JS #102 - account: ${accounts}`)
        if (accounts) {
          this.walletAddress = accounts[0]
          this.shortWalletAddress = this.truncateAddress(this.walletAddress)
          this.accounts = accounts
        }

        this.chainId = network.chainId
        this.network = network
        this.isConnected = true

        if (this.chainId == 69) {
          this.nativeCurrency = 'ETH'
          this.CONTRACT_ADDRESS = OPT_CONTRACT_ADDRESS
          this.contract = new ethers.Contract(
            this.CONTRACT_ADDRESS,
            abi,
            this.signer,
          )
        } else {
          
          this.nativeCurrency = 'MATIC'
          this.CONTRACT_ADDRESS = POLY_CONTRACT_ADDRESS
          this.contract = new ethers.Contract(
            this.CONTRACT_ADDRESS,
            abi,
            this.signer,
          )
        }

        console.log(` - chainId: ${this.chainId}`)
        console.log(` - network: ${this.network}`)
        console.log(` - walletAddress #120 ${this.walletAddress}`)
        console.log(` - Short WalletAddress #121 ${this.shortWalletAddress}`)
      } catch (error) {}
    },

    truncateAddress(address) {
      if (!address) return 'No Account'
      const match = address.match(
        /^(0x[a-zA-Z0-9]{4})[a-zA-Z0-9]+([a-zA-Z0-9]{4})$/,
      )
      if (!match) return address
      return `${match[1]}…${match[2]}`
    },
    toHex(num) {
      const val = Number(num)
      return '0x' + val.toString(16)
    },
    refreshState() {
      this.account = undefined
      this.accounts = undefined
      this.chainId = undefined
      this.network = undefined
      this.walletAddress = undefined
      this.shortWalletAddress = undefined
      this.provider = undefined
      this.isConnected = false
      //this.message = '';
      //this.signature = '';
      //this.verified = undefined;
    },
    async disconnect() {
      console.log('disconnecting ...')
      this.refreshState()
    },
    setWalletAddress(address) {
      console.log(` AccountStore #70 - ${address}`)
      this.walletAddress = address
    },
  },
})

import { defineStore } from "pinia";
import axios from "axios";
export const useBOOKINGSStore = defineStore("BOOKINGS", {
  state: () => ({
    root: "",
    id: 0,
    booking: {},
    procedures: [],
    sizeOfProcedures: 0,
    filProcedures: [],
    searchkey: "",
    all: [
      {
        tq_id:635,
        medicare_provider_id:220086,
        provider:"Beth Israel Deaconess Medical Center",
        health_system:"Beth Israel Deaconess Medical Center",
        date:"September 11, 11:00PM",
        cpt:71046,
        description:"Radiologic examination, chest; 2 views",
        rev_code:null,
        rate:"97.08",
        raw_plan:"AETNA PPO",
        mapped_payer:"Aetna",
        payer_class:"Commercial"
     }
    ],
  }),

  getters: {
    getBOOKINGRoot(state) {
      return state.root;
    },
    getBOOKINGById: (state) => {
      return (bookingId) =>
        state.all.find((booking) => booking.id === bookingId);
    },
  },

  actions: {
    async searchProcedures() {
      try {
        
        console.log(`Search Key in store bookings: ${this.searchkey}`)

        const searchData = {
          searchkey: this.searchkey,
        };


        const data = await axios.post(
          "http://localhost:3002/search",
          searchData
        );

  

        this.procedures = await data.data;
        this.sizeOfProcedures = this.procedures.length;
        console.log(`in store bookings js: print procedures ${JSON.stringify(this.procedures)}`)
        //this.searchkey = "";

        this.$router.push({ path: '/search', replace: true })

      } catch (error) {
        alert(error);
        console.log(error);
      }
    },
    setBOOKINGRoot(path) {
      //console.log(` DAOSStore #85 - ${path}`);
      this.root = path;
    },
    setBOOKINGId(id) {
      //console.log(` DAOSStore #89 - ${id}`);
      this.id = id;
    },
    setBOOKING(bookingId) {
      /* console.log(` DAOSStore #330 id is - ${daoId}`);
      if (isNaN(daoId)) {
        console.log(`DAOSStore - #332 : ${daoId} is not a number`)
      } else {
        console.log(`DAOSStore - #334 : ${daoId} is a number`)
      }
      console.log(` DAOSStore #336 ALL is - ${this.all}`); */
      //const myId = 2;
      //console.log(` DAOSStore #338 myId is - ${myId}`);
      //const found = this.all.filter(dao => dao.id === myId)
      const found = this.all.find((element) => element.id === bookingId);
      //console.log(` DAOSStore #340 myId is - ${myId}`);
      //console.log(` DAOSStore #344 found is - ${found}`);
      if (found) {
        this.booking = found;
      }
    },
  },
});

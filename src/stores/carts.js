import { defineStore } from "pinia";

export const useCARTSStore = defineStore("CARTS", {
  state: () => ({
    root: "",
    id: 0,
    cart: 0,
    dialog:false,
    dialog1:false,
  }),

  getters: {},

  actions: {
    addToCart() {
      console.log(this.cart);
      this.cart++;
      this.dialog = true;
      this.dialog1=false;
    },
    removeFromCart() {
      console.log(this.cart);
      this.cart--;
    },
    checkout(){
      this.$router.push({name: 'cart'});
      this.dialog = false;
      this.dialog1=false;
    },
    continueShopping(){
      this.$router.push({name: 'search'});
      this.dialog = false;
      this.dialog1=false;
    }
  },
});

import { defineStore } from "pinia";

export const useDAOSStore = defineStore("DAOS", {
  state: () => ({
    root: "",
    id: 0,
    dao: {},
    all: [
      {
        id: 1,
        name: "Raiseblock",
        src: require("@/assets/raiseblockcover.png"),
        members: "70",
        category: "nonprofit",
      },
      {
        id: 2,
        name: "Raiseblock CrowdFund",
        src: require("@/assets/raiseblockcover.png"),
        members: "70",
        category: "business",
      },
      {
        id: 3,
        name: "Sylo",
        src: require("@/assets/sylo_mark.png"),
        members: "70",
        category: "organization",
      },
      {
        id: 4,
        name: "Raiseblock CrowdLoan",
        src: require("@/assets/raiseblockcover.png"),
        members: "70",
        category: "lending",
      },
      {
        id: 5,
        name: "Totalfolio",
        src: require("@/assets/totalfolio_mark.png"),
        members: "70",
        category: "capital-market",
      },
    ],
    categories:[
      {
        id:1,
        name: "Nonprofit Organization",
        src:  require("@/assets/nonprofit.png"),
        link: '/crowdfunding/app/setup',
        description: 'Ex: donate to a charity'
      },
      {
        id:2,
        name: "Organisation",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex: donate to a charity or a business'
      },
      {
        id:3,
        name: "Lending",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex: loan to borrowers or causes'
      },
      {
        id:4,
        name: "Primary Market",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex: invest in an island from an issuer or the creation of a new television series '

      },
      {
        id:5,
        name: "Secondary Market",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex: invest in stock, bonds, options, futures, etc.'
      },
      {
        id:6,
        name: "Business",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex:  donate to a business'
      },
      {
        id:7,
        name: "Capital Market",
        src:  require("@/assets/nonprofit.png"),
        description: 'Ex: invest in primary or secondary markets'
      }
    ],
    impact:[
      {
        id:1,
        name: "Total Donations",
        value:  "200",
      },
      {
        id:2,
        name: "Total Granted",
        value:  "140",
      },
      {
        id:3,
        name: "Supported Nonprofits",
        value:  "3",
      },
      {
        id:4,
        name: "Projects",
        value:  15,
      },
      {
        id:5,
        name: "Towards creative work",
        value:  "26.8 AVAX",
      },
      {
        id:6,
        name: "Backings",
        value:  10,
      }
    ],
    charity:[
      {
        id:1,
        name: "Total Donations",
        value:  "2000",
      },
      {
        id:2,
        name: "Total Granted",
        value:  "1000",
      },
      {
        id:3,
        name: "Supported Nonprofits",
        value:  "3",
      },
    ],
    spaces:[
      {
        id:1,
        name: "SYLO",
        category:  "Organization",
        src:  require("@/assets/nonprofit.png"),
      },
      {
        id:2,
        name: "LendAsset",
        category:  "Lending",
        src:  require("@/assets/nonprofit.png"),
      },
      {
        id:3,
        name: "TotalFolio",
        category:  "Market" ,
        src:  require("@/assets/totalfolio_logo.png"),
      },
    ],
    oss:[
      {
        id:1,
        title: "suiundo/TemplesDAO",
        avatar:"",
        advised:"oss Project",
        impact:  "54.8k",
        value:50,
        address:  "ox2D69...87c1",
        grantable: '0.00',
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
    ],
    community:[
      {
        id:1,
        title: "Protect Reproductive Rights",
        impact:  "54.8k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        advised: "Raiseblock",
        grant: 7,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/raiseblockcover.png"),
        location: "Washington, DC",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:20,

      },
      {
        id:2,
        title: "End Gun Violence",
        impact:  "7.0k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        advised: "Raiseblock",
        grant: 1,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/raiseblockcover.png"),
        location: "New York, NY",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:80,
      },
      {
        id:3,
        title: "COWGIRLDAO",
        impact:  "27.5k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        advised: "Raiseblock",
        grant: 1,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/raiseblockcover.png"),
        location: "New York, NY",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:90,
      },
    ],
    organization:[
      {
        id:1,
        title: "Choice USA",
        donate:  "7.7k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        grant: 7,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/choice.gif"),
        icon: "mdi-scale-unbalanced",
        ein: "52-1772575",
        location: "Washington, DC",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:97,
      },
      {
        id:2,
        title: "Planned Parenthood Federation of America",
        donate:  "32.7k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        grant: 1,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/Planned_Parenthood.svg"),
        icon: "mdi-clipboard",
        ein: "13-1644147",
        location: "New York, NY",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:100,
      },
      {
        id:3,
        title: "Fos Ferminista",
        donate:  "7.7k",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
        grant: 1,
        grantable: '0.00',
        category: "nonprofit organization",
        src: require("@/assets/FOS.png"),
        icon: "mdi-clipboard",
        ein: "13-1845455",
        location: "New York, NY",
        revenue: "293.7m",
        expenses: "341.2m",
        income: "47.4m",
        assets: "413.5m",
        liability: "88.7m",
        netAsset:"324.7m",
        program: "71.65",
        administrative:"12.1",
        fundraising:"16.26",
        capital: "0.952 yrs",
        value:50,
      },
    ],
    women:[
      {
        id:1,
        name:"Neveen",
        amount:"2,550",
        days: 34,
        percentage: 30,
        description: "for medical treatment",
        location: "PALESTINE",
        src: require('@/assets/womenloan2.jpg'),
        purpose: 'Health',
        loan:6000,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
      {
        id:2,
        name:"Jessica",
        amount:"3,550",
        days: 31,
        percentage: 40,
        description: "for mortgage.",
        location: "USA",
        src: require('@/assets/womenloan1.jpg'),
        purpose: 'Mortgage',
        loan:5000,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
      {
        id:3,
        name:"Kimya Group",
        amount:"550",
        days: 30,
        percentage:  25,
        description: "for health.",
        location: "CONGO(DRC)",
        src: require('@/assets/womenloan3.jpg'),
        purpose: 'Health',
        loan:2000,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
    ],
    refugee:[
      {
        id:1,
        name:"Eduar Enrique",
        amount:"55",
        days: 29,
        percentage: 95,
        description: "to setup a store.",
        location: "COLOMBIA",
        src: require('@/assets/refugee1.jpg'),
        purpose: 'Grocery Store',
        loan:60,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
      {
        id:2,
        name:"Abdullah",
        amount:"1,550",
        days: 31,
        percentage:50,
        description: "to get farm equipments.",
        location: "PALESTINE",
        src: require('@/assets/refugee2.jpg'),
        purpose: 'Farm Equipments',
        loan:3000,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
      {
        id:3,
        name:"Abeer",
        amount:"450",
        days: 34,
        percentage:  20,
        description: "for health.",
        location: "JORDAN",
        src: require('@/assets/refugee3.jpg'),
        purpose: 'Health',
        loan:1500,
        story: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi explicabo provident unde praesentium laboriosam ipsam nisi molestias repudiandae sint sequi, soluta odit exercitationem eiusex. Exercitationem consequatur optio illum explicabo.",
      },
    ],
    womenFounders: [
      {
        id: 1,
        name: "Women Founders",
        src: require("@/assets/women.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    music: [
      {
        id: 1,
        name: "Music",
        src: require("@/assets/music.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
      {
        id: 2,
        name: "Music",
        src: require("@/assets/music1.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
      {
        id: 3,
        name: "Music",
        src: require("@/assets/music2.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    socialImpact: [
      {
        id: 1,
        name: "Social Impact",
        src: require("@/assets/charity.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
      {
        id: 2,
        name: "Social Impact",
        src: require("@/assets/charity1.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    combatCarbon: [
      {
        id: 1,
        name: "Combat Carbon",
        src: require("@/assets/carbon.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    sustainability: [
      {
        id: 1,
        name: "Sustainability",
        src: require("@/assets/carbon.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    disaster: [
      {
        id: 1,
        name: "Disaster Relief",
        src: require("@/assets/carbon.jpeg"),
        members: "60k",
        category: "nonprofit",
      },
    ],
    treasury: [
      {
        id: 1,
        title: "Recovering funds impacted by the unsupported Bitcoin version bug",
        src: require("@/assets/avatar.png"),
        kint:"400",
        category: "treasury",
        address: "a3bB...V5nE",
        days:"14d",
        comment:2,
        status: "Proposed",
        color: "blue"
      },
      {
        id: 3,
        title: "Untitled - treasury proposal #3",
        src: require("@/assets/avatar.png"),
        kint:"0.15",
        category: "treasury",
        address: "a3aD...m9ni",
        days:"3mos",
        comment:0,
        status: "Proposed",
        color: "blue"
      },
      {
        id: 5,
        title: "Untitled - treasury proposal #5",
        src: require("@/assets/avatar.png"),
        kint:"7",
        category: "treasury",
        address: "a3aD...m9ni",
        days:"3mos",
        comment:0,
        status: "Proposed",
        color: "blue"
      },
    ],
    tech: [
      {
        id: 47,
        title: "Untitled - public proposal #47",
        src: require("@/assets/avatar.png"),
        kint:"9",
        category: "tech",
        address: "RaiseBlock / Interlay",
        days:"7d",
        comment:0,
        status: "Executed",
        color: "green"
      },
    ],
    referenda: [
      {
        id: 20,
        title: "Untitled - public proposal #20",
        src: require("@/assets/avatar.png"),
        kint:"9",
        category: "referenda",
        address: "RaiseBlock / Interlay",
        days:"7d",
        comment:0,
        status: "Started",
        color: "blue"
      },
    ],
    democracy: [
      {
        id: 7,
        title: "Untitled - public proposal #7",
        src: require("@/assets/avatar.png"),
        kint:"9",
        category: "referenda",
        address: "RaiseBlock / Interlay",
        days:"7d",
        comment:0,
        status: "FastTracked",
        color: "green",
        proposals: 0,
        referenda: 1,
        launch: "1 day 8 hrs"
      },
    ],
    discussion: [
      {
        id: 7,
        title: "Merge iBTC vs kBTC",
        src: require("@/assets/avatar.png"),
        category: "referenda",
        address: "a3aD...m9ni",
        days:"17d",
        comment:0,
      },
      {
        id: 10,
        title: "Merge iBTC vs kBTC",
        src: require("@/assets/avatar.png"),
        category: "referenda",
        address: "a3aD...m9ni",
        days:"17d",
        comment:0,
      },
      {
        id: 11,
        title: "Merge iBTC vs kBTC",
        src: require("@/assets/avatar.png"),
        category: "referenda",
        address: "a3aD...m9ni",
        days:"17d",
        comment:0,
      },
    ],
    member: [
      {
        id: 11,
        title: "MEMBERS",
        src: require("@/assets/avatar.png"),
        address: "a3aD...m9ni",
      },
    ],
  }),

  getters: {
    getDAORoot(state) {
      return state.root;
    },
    getDAOById: (state) => {
      return (daoId) => state.all.find((dao) => dao.id === daoId);
    },
    getTREASURYById: (state) => {
      return (daoId) => state.treasury.find((dao) => dao.id === daoId);
    },
    getTECHById: (state) => {
      return (daoId) => state.tech.find((dao) => dao.id === daoId);
    },
    getREFERENDAById: (state) => {
      return (daoId) => state.referenda.find((dao) => dao.id === daoId);
    },
    getDEMOCRACYById: (state) => {
      return (daoId) => state.democracy.find((dao) => dao.id === daoId);
    },
    getDISCUSSIONById: (state) => {
      return (daoId) => state.discussion.find((dao) => dao.id === daoId);
    },
    getMEMBERById: (state) => {
      return (daoId) => state.member.find((dao) => dao.id === daoId);
    },
     /* getDAOById: (state) => {
      return (daoId) => state.all.find((dao) => dao.id === daoId);
    }, */
    getOSSById: (state) => {
      return (daoId) => state.oss.find((dao) => dao.id === daoId);
    },
    getCOMById: (state) => {
      return (daoId) => state.community.find((dao) => dao.id === daoId);
    },
    getORGById: (state) => {
      return (daoId) => state.organization.find((dao) => dao.id === daoId);
    },
    getWOMENById: (state) => {
      return (daoId) => state.women.find((dao) => dao.id === daoId);
    },
    getREFUGEEById: (state) => {
      return (daoId) => state.refugee.find((dao) => dao.id === daoId);
    },
    getWOMENFOUNDERSById: (state) => {
      return (daoId) => state.womenFounders.find((dao) => dao.id === daoId);
    },
    getMUSICById: (state) => {
      return (daoId) => state.music.find((dao) => dao.id === daoId);
    },
    getSOCIALIMPACTById: (state) => {
      return (daoId) => state.socialImpact.find((dao) => dao.id === daoId);
    },
    getCOMBATCARBONById: (state) => {
      return (daoId) => state.combatCarbon.find((dao) => dao.id === daoId);
    },
  },

  actions: {
    setDAORoot(path) {
      //console.log(` DAOSStore #85 - ${path}`);
      this.root = path;
    },
    setDAOId(id) {
      //console.log(` DAOSStore #89 - ${id}`);
      this.id = id;
    },
    setDAO(daoId) {
      /* console.log(` DAOSStore #330 id is - ${daoId}`);
      if (isNaN(daoId)) {
        console.log(`DAOSStore - #332 : ${daoId} is not a number`)
      } else {
        console.log(`DAOSStore - #334 : ${daoId} is a number`)
      }
      console.log(` DAOSStore #336 ALL is - ${this.all}`); */
      //const myId = 2;
      //console.log(` DAOSStore #338 myId is - ${myId}`);
      //const found = this.all.filter(dao => dao.id === myId)
      const found = this.all.find(element => element.id === daoId);
      //console.log(` DAOSStore #340 myId is - ${myId}`);
      //console.log(` DAOSStore #344 found is - ${found}`);
      if(found) {
        this.dao = found;
      }
      
    },
  },
});

import { defineStore } from "pinia";

export const usePAYMENTSStore = defineStore("PAYMENTS", {
  state: () => ({
    root: "",
    id: 0,
    payment: {},
    cash: 21.00,
    amount: 0.00,
    
  }),

  getters: {
    
  },

  actions: {
    cashUpdate() {
      console.log(this.cash);
     console.log(this.amount);
    //  this.cash= Number(this.cash).toFixed(2) + Number(this.amount).toFixed(2)
    this.cash= Number(this.cash + this.amount).toFixed(2)
     console.log(this.cash);
     console.log(this.amount);
     
    },
    async createTerminalCheckout() {
      const paymentData = {
        amount: this.amount,
        note: this.requestNote,
        patientnum: this.patientId,
      };

      console.log(`Payment Data is: ${paymentData}`);

      const res = await axios.post(
        "https://billpractiice.herokuapp.com/terminal/checkout",
        paymentData
      );

      console.log(`response - ${res.data}`);

      this.dialog = true;
    },
  },
});

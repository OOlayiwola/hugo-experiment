import { defineStore } from "pinia";

export const usePROFILESStore = defineStore("PROFILES", {
  state: () => ({
    root: "",
    id: 0,
    profile: {},
    all: [
        {
            id:"1",
            profile: "Curly Howard",
            name: "Curly",
            role: "Customer",
            link: "/customer",
            src: require('@/assets/avatar.png'),
            email:"miriam@examplemail.com",
            currency:"USD",
            language:"English",
            timezone: "Central Time"
          },
          {
            id:"2",
            profile: "Olivia Green",
            name: "Olivia",
            role: "Bank",
            link: "/bank",
            src: require('@/assets/avatar1.png'),
            email:"olivia@examplemail.com",
            currency:"USD",
            language:"English",
            timezone: "Central Time"
          },
          // {
          //   id:"3",
          //   profile: "Jennifer Wu",
          //   name: "Jennifer",
          //   role: "Payer",
          //   link: "/payer",
          //   src: require('@/assets/avatar1.png'),
          //   email:"jen@examplemail.com",
          //   currency:"USD",
          //   language:"English",
          //   timezone: "Central Time"
          // },
    ],
    
  }),

  getters: {
    getPROFILERoot(state) {
      return state.root;
    },
    getPROFILEById: (state) => {
      return (profileId) => state.all.find((profile) => profile.id === profileId);
    },
  },

  actions: {
    setPROFILERoot(path) {
      //console.log(` DAOSStore #85 - ${path}`);
      this.root = path;
    },
    setPROFILEId(id) {
      //console.log(` DAOSStore #89 - ${id}`);
      this.id = id;
    },
    setPROFILE(profileId) {
      /* console.log(` DAOSStore #330 id is - ${daoId}`);
      if (isNaN(daoId)) {
        console.log(`DAOSStore - #332 : ${daoId} is not a number`)
      } else {
        console.log(`DAOSStore - #334 : ${daoId} is a number`)
      }
      console.log(` DAOSStore #336 ALL is - ${this.all}`); */
      //const myId = 2;
      //console.log(` DAOSStore #338 myId is - ${myId}`);
      //const found = this.all.filter(dao => dao.id === myId)
      const found = this.all.find(element => element.id === profileId);
      //console.log(` DAOSStore #340 myId is - ${myId}`);
      //console.log(` DAOSStore #344 found is - ${found}`);
      if(found) {
        this.profile = found;
      }
      
    },
  },
});

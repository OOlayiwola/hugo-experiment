import { defineStore } from "pinia";

export const useSETTINGSStore = defineStore("SETTINGS", {
  state: () => ({
    root: "",
    id: 0,
    setting: {},
    cashApp: false,
    card: false,
    bank: false,
    invoice: false,
    insurance: false,
    
  }),

  getters: {
    
  },

  actions: {
   
  },
});
